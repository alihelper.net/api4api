const fetch = require('node-fetch')
let URL = (process.env.API4API_URL == null) ? 'http://localhost:8888/api' : process.env.API4API_URL;

api4api = {
    send: async (method, params, id, headers, options) => {
        let reconnectTries = 1;
        let timeout = 0;
        if (options != undefined) {
            if (options['reconnectTries'] !== undefined) {
                reconnectTries = parseInt(options['reconnectTries']);
            }
            if (options['timeout'] !== undefined) {
                timeout = parseInt(options['timeout']);
            }
        }
        let request = { jsonrpc: '2.0', method: method, params: params, id: id };
        if (headers === null) {
            headers = { 'Content-Type': 'application/json' };
        }
        for (let index = 0; index < reconnectTries; index++) {
            try {
                const res = await fetch(URL, { 
                    method: 'POST',
                    body:    JSON.stringify(request),
                    headers: headers,
                    timeout: timeout,
                });
                const json = await res.json();
                return {
                    value: json,
                    err: null
                };
            } catch(e) {
                if (index >= (reconnectTries - 1)) {
                    return {
                        value: null,
                        err: Error(e.message)
                    };
                } else {
                    console.log(e.message, index, 'try again...')
                }
            }
        }
    },
    generateID: () => {
        return Math.floor(Math.random() * (Number.MAX_SAFE_INTEGER - 1 + 1)) + 1;
    }
}

module.exports = api4api;