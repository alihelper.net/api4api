# Library to send JsonRPC request

First set API4API_URL in your environment variables 

# How to use

```
    const api4api = require('api4api');

    let generatedID = api4api.generateID();
    let headers = {
        'Content-Type': 'application/json'
    };
    let query = {
        id: 12345
    };
    let options = {reconnectTries: 5, timeout: 60000};
    let response = await api4api.send('GetProduct', query, generatedID, headers, options)
    if (response.err == null) {
        if (response.value.error != undefined) {
            throw new Error(`${response.value.error.message}: ${response.value.error.code} ${response.value.error.data}`)
        }
        if (response.value.id !== generatedID) {
            throw new Error(`Bad response: return ${response.value.id}, but need ${generatedID}`)
        }
        if(response.value.result == null) {
            return {};
        }
        return response.value.result;
    } else {
        throw response.err
    }
```